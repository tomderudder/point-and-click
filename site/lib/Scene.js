'use strict'

const STEP_RATIO_HORIZONTAL = 0.8
const STEP_RATIO_VERTICAL = 0.6
const CURSOR_MARGIN_LEFT = 100
const CURSOR_MARGIN_RIGHT = 100
const CURSOR_MARGIN_TOP = 25
const CURSOR_MARGIN_BOTTOM = 25

/**
 * Represents a scene.
 * @param {string} name - The name of the scene.
 * @param {number} width - The width of the scene.
 * @param {number} height - The height of the scene.
 * @param {object} options
 * @param {array} options.layers
 * @param {character} options.player
 * @param {array} options.areas
 * @param {array} options.audio
 */

class Scene {
  /**
   * @constructor
   * @param {string} name - The name of the scene.
   * @param {number} width - The width of the scene.
   * @param {number} height - The height of the scene.
   * @param {object} options
   * @param {array} options.layers
   * @param {character} options.player
   * @param {array} options.areas
   * @param {array} options.audio
   */

  constructor(
    name = 'scene ' + Scene.sceneCount,
    width = 1280,
    height = 720,
    { layers = [], player, areas = [], audio = [] }
  ) {
    this.name = name
    this.width = width
    this.height = height
    this.game = null
    this.character = null
    this.raf = null
    this.audios = [] // list of audio in the scene
    this.layers = [] // list of layers in the scene
    this.areas = new AreaManager(this.width, this.height)
    this.cursorPosition = new Vector(-50, -50)
    Scene.sceneCount++

    if (player) {
      this.setPlayerCharacter(player)
    }
    for (let i = 0; i < layers.length; i++) {
      this.addLayer(layers[i])
    }
    for (let i = 0; i < areas.length; i++) {
      this.addArea(areas[i])
    }
    for (let i = 0; i < audio.length; i++) {
      this.addAudio(audio[i])
    }
  }

  /**
   * Bind a externe method to the object
   * @param {function} method - The method to bind.
   */

  bind(method) {
    return event => this[method.name](event)
  }

  /**
   * @name onclick
   * Event handler code then a click event is keeping
   * @param {MouseEvent} event - The event object.
   */

  onclick(event) {
    const from = this.character.position
    let to = this.getAbsolutePositionFromEvent(event)

    action: for (const layer of this.layers)
      for (const character of layer.characters)
        if (character.component.contains(to)) {
          Scene.action = () => {
            character.onclick()
            Scene.action = null
          }
          break action
        }
    to = this.areas.getPositionClosest(to)

    this.areas.calculatePath(from, to, path => {
      this.character.path = path.map(e => [e[0] * 10, e[1] * 10])
    })
  }

  /**
   * Event handler code then a double click event is keeping
   * @param {MouseEvent} event - The event object.
   */
  ondblclick(event) {
    this.character.component.speed.x *= 2
    this.character.component.speed.y *= 2
  }

  /**
   * Event handler code then a mousse move event is keeping
   * @param {MouseEvent} event - The event object.
   */
  onmousemove(event) {
    this.cursorPosition = this.getRelatifPositionFromEvent(event)
    this.cursorSelected = this.getCursorDirectionFromEvent(event)
  }

  /**
   * @name getCursorDirectionFromEvent
   * Extract the cursor direction from position
   * @param {MouseEvent} event - The event object.
   */

  getCursorDirectionFromEvent(event) {
    const { offsetX: x, offsetY: y } = event

    if (x < CURSOR_MARGIN_LEFT) {
      // left
      return this.game.cursor.cursors.left
    } else if (this.canvas.width - x < CURSOR_MARGIN_RIGHT) {
      // right
      return this.game.cursor.cursors.right
    } else if (y < CURSOR_MARGIN_TOP) {
      // top
      return this.game.cursor.cursors.top
    } else if (this.canvas.height - y < CURSOR_MARGIN_BOTTOM) {
      // bottom
      return this.game.cursor.cursors.bottom
    } else {
      // middle
      return this.game.cursor.cursors.default
    }
  }

  /**
   * @name getRelatifPositionFromEvent
   * Extract the position without offset
   * @param {MouseEvent} event - The event object.
   */

  getRelatifPositionFromEvent(event) {
    const { offsetX: x, offsetY: y } = event
    const relPosition = new Vector()

    switch (this.game.display.scalMode) {
      case SCALE_MODE_RESIZE:
        relPosition.x = x / this.game.display.ratio.x
        relPosition.y = y / this.game.display.ratio.y
        break

      case SCALE_MODE_EXPAND:
      default:
        relPosition.x = x
        relPosition.y = y
        break
    }
    return relPosition
  }

  /**
   * @name getAbsolutePositionFromEvent
   * Extract the position with offset
   * @param {MouseEvent} event - The event object.
   */

  getAbsolutePositionFromEvent(event) {
    const { offsetX: x, offsetY: y } = event
    const absPosition = new Vector()

    switch (this.game.display.scalMode) {
      case SCALE_MODE_RESIZE:
        absPosition.x = x / this.game.display.ratio.x + Scene.offsetX
        absPosition.y = y / this.game.display.ratio.y + Scene.offsetY
        break

      case SCALE_MODE_EXPAND:
      default:
        absPosition.x = x + Scene.offsetX
        absPosition.y = y + Scene.offsetY
        break
    }
    return absPosition
  }

  /**
   * @name absPositionToRelPosition
   * Convert absolute position to relative
   * @param {Vector} position - The position object.
   * @param {Number} position.x - The x position value.
   * @param {Number} position.y - The y position value.
   * @param {Number} position.z - The z position value.
   */

  static absPositionToRelPosition(position) {
    const { x, y, z } = position

    return new Vector(x + Scene.offsetX, y + Scene.offsetY, z)
  }

  /**
   * @name relPositionToAbsPosition
   * Convert relative position to absolute
   * @param {Vector} position - The position object.
   * @param {Number} position.x - The x position value.
   * @param {Number} position.y - The y position value.
   * @param {Number} position.z - The z position value.
   */

  static relPositionToAbsPosition(position) {
    const { x, y, z } = position

    return new Vector(x - Scene.offsetX, y - Scene.offsetY, z)
  }

  /**
   * Synchronizes the state of the audio betwwen the scene and the game
   */
  updateStateAudio() {
    this.audios.forEach(audio => {
      if (audio.readyState) {
        audio.play().catch(err => {
          console.error(err)
        })
      }
      audio.muted = this.game.muted
    })
  }

  /**
   * Add a audio in the scene
   * @param {Sound} audio - The audio to add.
   */
  addAudio(audio) {
    audio.onended = () => audio.play()
    this.audios.push(audio)
  }

  /**
   * Add a layer in the scene
   * @param {Layer} layer - The layer to add.
   */
  addLayer(layer) {
    this.layers.push(layer)
    this.layers.sort((layerA, layerB) => {
      return layerA.zIndex - layerB.zIndex
    })
  }

  /**
   * Add a area in the scene
   * @param {Area} area - The area to add.
   */
  addArea(area) {
    this.areas.add(area)
  }

  /**
   * Initialize the player character in the scene
   * @param {character} player - The character to add.
   */
  setPlayerCharacter(player) {
    this.character = player
  }

  /**
   * Load the scene in the canvas
   */
  load() {
    if (!this.character) return console.error('no character')

    this.character.setToInitalPosition()
    this.setToInitalPosition()
    this.updateStateAudio()
    this.draw()
  }

  /**
   * Unload the scene in the canvas
   */
  unload() {
    this.cursorPosition = new Vector(-50, -50)
    this.clearCanvas()
    this.audios.forEach(audio => {
      audio.pause()
      audio.load()
    })
    this.character && this.character.stop()
    window.cancelAnimationFrame(this.raf)
  }

  /**
   * Clear the canvas
   */
  clearCanvas() {
    this.ctx.clearRect(
      0,
      0,
      this.canvas.width / this.game.display.ratio.x,
      this.canvas.height / this.game.display.ratio.y
    )
  }

  /**
   * Draw and update the scene
   */
  draw() {
    // pinting
    const callback = () => {
      this.areas.draw(this.ctx)
    }
    this.character.isDrawn = false
    this.clearCanvas()
    this.layers.forEach(layer => {
      if (layer.zIndex > this.character.zIndex) this.character.draw(this.ctx, callback) // draw area and character
      layer.components.forEach(component => {
        component.draw(this.ctx)
      })
      layer.characters.forEach(character => {
        character.component.draw(this.ctx)
      })
    })
    this.character.draw(this.ctx, callback) // draw area and character
    this.layers.forEach(layer => {
      layer.characters.forEach(character => character.textDraw(this.ctx))
    })
    // console.log(this.game.cursor.cursorSelected)
    CursorManager.draw(this.cursorSelected, this.cursorPosition) // draw cursor

    // calcul
    this.character.component.update()
    this.characterGoTop() // sliding
    this.characterGoBottom()
    this.characterGoLeft()
    this.characterGoRight()
    const activeAareas = this.areas.getActiveArea(this.character.position)
    for (const area of activeAareas) if (area.onOver && !area.onOver()) return

    // self-calling
    this.raf = window.requestAnimationFrame(this.bind(this.draw))
  }

  /**
   * @name setToInitalPosition
   * Initializes the camera position
   */
  setToInitalPosition() {
    switch (this.game.display.scalMode) {
      case SCALE_MODE_RESIZE:
        Scene.offsetX = this.width - this.canvas.width / this.game.display.ratio.x
        Scene.offsetY = this.height - this.canvas.height / this.game.display.ratio.y
        break
      case SCALE_MODE_EXPAND:
        Scene.offsetX = this.width - this.canvas.width
        Scene.offsetY = this.height - this.canvas.height
        break
      default:
        Scene.offsetX = 0
        Scene.offsetY = 0
        break
    }
  }

  /**
   * Move the camera position then player go to top
   */
  characterGoTop() {
    const moveStep = (this.canvas.height / this.game.display.ratio.y) * STEP_RATIO_VERTICAL
    const delta = this.character.component.position.y - Scene.offsetY
    if (delta < 0) {
      if (Scene.offsetY > moveStep) Scene.offsetY -= moveStep
      else Scene.offsetY -= Scene.offsetY
    }
  }

  /**
   * Move the camera position then player go to right
   */
  characterGoRight() {
    const moveStep = (this.canvas.width / this.game.display.ratio.x) * STEP_RATIO_HORIZONTAL
    const delta = Math.abs(
      Scene.offsetX +
        this.canvas.width / this.game.display.ratio.x -
        this.character.component.position.x -
        this.character.component.width / 2
    )
    if (delta < this.character.component.width / 2 + CURSOR_MARGIN_RIGHT) {
      const a = Scene.offsetX + this.canvas.width / this.game.display.ratio.x
      if (a != this.width) {
        if (a + moveStep < this.width) Scene.offsetX += moveStep
        else Scene.offsetX += this.width - a
      }
    }
  }

  /**
   * Move the camera position then player go to bottom
   */
  characterGoBottom() {
    const moveStep = (this.canvas.height / this.game.display.ratio.y) * STEP_RATIO_VERTICAL
    const delta = Math.abs(
      Scene.offsetY +
        this.canvas.height / this.game.display.ratio.y -
        this.character.component.position.y -
        this.character.component.height
    )
    if (delta < 50) {
      let a
      switch (this.game.display.scalMode) {
        case SCALE_MODE_RESIZE:
          a = Scene.offsetY + this.canvas.height / this.game.display.ratio.y
          break
        case SCALE_MODE_EXPAND:
          a = Scene.offsetY + this.canvas.height
          break
      }
      if (a != this.height) {
        if (a + moveStep < this.height) Scene.offsetY += moveStep
        else Scene.offsetY += this.height - a
      }
    }
  }

  /**
   * Move the camera position then player go to left
   */
  characterGoLeft() {
    const moveStep = (this.canvas.width / this.game.display.ratio.x) * STEP_RATIO_HORIZONTAL
    const delta = Math.abs(Scene.offsetX + 0 - this.character.component.position.x)

    if (delta < this.character.component.width / 2 + CURSOR_MARGIN_LEFT) {
      if (Scene.offsetX > moveStep) Scene.offsetX -= moveStep
      else Scene.offsetX -= Scene.offsetX
    }
  }

  /**
   * cursorSelected
   * @param {Sprite} cursor
   */

  set cursorSelected(cursor) {
    this.game.cursor.cursorSelected = cursor
  }

  /**
   * cursorSelected
   * @return {Sprite} cursor
   */

  get cursorSelected() {
    return this.game.cursor.cursorSelected
  }
}

/**
 * Static properties
 */
Scene.sceneCount = 0
Scene.offsetX = 0
Scene.offsetY = 0
Scene.action = null
