'use strict'

/**
 * Represents a circular area.
 * @param {string} name - The name of the area.
 * @param {number} type - The type of area.
 * @param {object} coordinates - The coordinates.
 * @param {function} onOver - The onOver event.
 */
class CircleArea extends Area {
  /**
   * @constructor
   * @param {string} name - The name of the area.
   * @param {number} type - The type of area.
   * @param {object} coordinates - The coordinates.
   * @param {function} onOver - The onOver event.
   */
  constructor(name, type, coordinates, onOver) {
    super(name, type, AREA_CIRCLE, coordinates, onOver)
  }

  /**
   * Draw the area.
   * @param {CanvasRenderingContext2D} ctx - The canvas 2D context.
   */
  draw(ctx) {
    CircleArea.draw(ctx, this)
  }

  /**
   * Draw a area.
   * @param {CanvasRenderingContext2D} ctx - The canvas 2D context.
   * @param {CircleArea} ctx - The area.
   */
  static draw(ctx, area) {
    ctx.arc(
      area.coordinates.center.x - Scene.offsetX,
      area.coordinates.center.y - Scene.offsetY,
      area.coordinates.radius,
      0,
      2 * Math.PI
    )
  }

  /**
   * Returns if a specified position occurs within the area.
   * @param {Vector} position - The position to seek.
   */
  contains(position) {
    return CircleArea.contains(position, this)
  }

  /**
   * Returns if a specified position occurs within a area.
   * @param {Vector} position - The position to seek.
   * @param {CircleArea} ctx - The area.
   */
  static contains(position, area) {
    /*
     * need to code
     */

    return false
  }
}
