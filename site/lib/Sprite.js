'use strict'

/**
 * Represents a sprite.
 * @param {string} src - The address of the resource.
 * @param {number} x - The x position in the canvas.
 * @param {number} y - The y position in the canvas.
 * @param {number} width - The width of the image.
 * @param {number} height - The height of the image.
 */

class Sprite extends Image {
  /**
   * @constructor
   * @param {string} src - The address of the resource.
   * @param {number} x - The x position in the canvas.
   * @param {number} y - The y position in the canvas.
   * @param {number} width - The width of the image.
   * @param {number} height - The height of the image.
   */
  constructor(src, x = 0, y = 0, width, height) {
    super()

    this.src = src
    this.position = new Vector(x, y)
    this.direction = new Vector(0, 0)
    this.speed = new Vector(0, 0)

    if (width) this.width = width
    if (height) this.height = height

    this.coordinates = {
      x: this.position.x,
      y: this.position.y,
      w: this.width,
      h: this.height
    }
  }

  /**
   * Draw the sprite.
   * @param {CanvasRenderingContext2D} ctx - The canvas 2D context.
   */
  draw(ctx) {
    ctx.drawImage(
      this,
      this.position.x - Scene.offsetX,
      this.position.y - Scene.offsetY,
      this.width,
      this.height
    )
  }

  /**
   * Update the state of the sprite.
   */
  update() {
    this.position.x += this.direction.x * this.speed.x
    this.position.y += this.direction.y * this.speed.y
  }

  /**
   * Returns if a specified position occurs within the sprite.
   * @param {Vector} position - The position to seek.
   */
  contains(position) {
    return Rectangle.contains(position, this.coordinates)
  }
}
