'use strict';

const $ = query => {
    const selector = document.querySelectorAll(query);
    return (selector.length > 1) ? Array.from(selector) : selector[0];
}
const $id = id => document.getElementById(id)
const $class = className => document.getElementsByClassName(className);
