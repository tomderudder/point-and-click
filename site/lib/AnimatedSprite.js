'use strict'

/**
 * Represents a animated sprite.
 * @param {string} src - The address of the resource.
 * @param {number} x - The x position in the canvas.
 * @param {number} y - The y position in the canvas.
 * @param {number} width - The width of the image.
 * @param {number} height - The height of the image.
 * @param {number} rows - The number of direction in the sprite.
 * @param {number} columns - The number of frames in the sprite.
 */

class AnimatedSprite extends Sprite {
  /**
   * @constructor
   * @param {string} src - The address of the resource.
   * @param {number} x - The x position in the canvas.
   * @param {number} y - The y position in the canvas.
   * @param {number} width - The width of the image.
   * @param {number} height - The height of the image.
   * @param {number} rows - The number of direction in the sprite.
   * @param {number} columns - The number of frames in the sprite.
   */
  constructor(src, x, y, width = 0, height = 0, rows = 1, columns = 1) {
    super(src, x, y, width / columns, height / rows)

    this.frameSelected = { col: 0, row: 0 }
    this.intervalId = null
    this.intervalDirection
    this.cols = columns
  }

  /**
   * Draw the sprite
   * @param {CanvasRenderingContext2D} ctx - The canvas 2D context.
   */
  draw(ctx) {
    /** select frame in animation */
    const srcX = this.frameSelected.col * this.width
    /** select animation direction */
    const srcY = this.frameSelected.row * this.height

    ctx.drawImage(
      this,
      srcX,
      srcY,
      this.width,
      this.height,
      this.position.x - Scene.offsetX,
      this.position.y - Scene.offsetY,
      this.width,
      this.height
    )
  }

  /**
   * Animated the sprite
   */
  animeMoveLeft() {
    if (this.intervalDirection === false) this.stop()

    if (this.intervalId === null || this.intervalDirection === false) {
      this.intervalDirection = true
      this.frameSelected.row = 1
      this.frameSelected.col =
        --this.frameSelected.col >= 0 ? this.frameSelected.col : this.cols - 2
      this.intervalId = setInterval(() => {
        this.frameSelected.col =
          --this.frameSelected.col >= 0 ? this.frameSelected.col : this.cols - 2
      }, 160)
    }
  }

  /**
   * Animated the sprite
   */
  animeMoveRight() {
    if (this.intervalDirection === true) this.stop()

    if (this.intervalId === null || this.intervalDirection === true) {
      this.intervalDirection = false
      this.frameSelected.row = 0
      this.frameSelected.col = ++this.frameSelected.col
      if (this.frameSelected.col >= this.cols) this.frameSelected.col = 1
      this.intervalId = setInterval(() => {
        this.frameSelected.col = ++this.frameSelected.col
        if (this.frameSelected.col >= this.cols) this.frameSelected.col = 1
      }, 160)
    }
  }

  /**
   * Stop the animation of the sprite
   */
  stop() {
    clearInterval(this.intervalId)
    this.intervalId = null
    this.frameSelected.col = this.frameSelected.row * (this.cols - 1)
  }
}
