'use strict'

const WALKABLE_AREA = 0
const SWITCHABLE_AREA = 1
const AREA_RECT = 0
const AREA_CIRCLE = 1

/**
 * Represents a area.
 * @param {string} name - The name of the area.
 * @param {number} type - The type of the area.
 * @param {number} shapeType - The the type of shape of the area.
 * @param {object} coordinates - The coordinates.
 * @param {function} onOver - The onOver event.
 */
class Area {
  /**
   * @constructor
   * @param {string} name - The name of the area.
   * @param {number} type - The type of the area.
   * @param {number} shapeType - The the type of shape of the area.
   * @param {object} coordinates - The coordinates.
   * @param {function} onOver - The onOver event.
   */
  constructor(name = 'area ' + Area.areaCount, type, shapeType, coordinates, onOver) {
    this.name = name
    this.type = type
    this.shapeType = shapeType
    this.coordinates = coordinates
    Area.areaCount++
    if (onOver) this.onOver = onOver
  }

  /**
   * Draw the area.
   * @param {CanvasRenderingContext2D} ctx - The canvas 2D context.
   */
  draw(ctx) {
    switch (this.shapeType) {
      case AREA_RECT:
        RectArea.draw(ctx, this)
        break
      case AREA_CIRCLE:
        CircleArea.draw(ctx, this)
        break
    }
  }

  /**
   * Returns if a specified position occurs within the area.
   * @param {Vector} position - The position to seek.
   */
  contains(position) {
    switch (this.shapeType) {
      case AREA_RECT:
        return RectArea.contains(position, this)
      case AREA_CIRCLE:
        return CircleArea.contains(position, this)
    }
    return false
  }
}

/**
 * static properties
 */
Area.areaCount = 0
