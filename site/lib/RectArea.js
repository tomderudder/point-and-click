'use strict'

/**
 * Represents a rectangular area.
 * @param {string} name - The name of the area.
 * @param {number} type - The type of area.
 * @param {object} coordinates - The coordinates.
 * @param {function} onOver - The onOver event.
 */
class RectArea extends Area {
  /**
   * @constructor
   * @param {string} name - The name of the area.
   * @param {number} type - The type of area.
   * @param {object} coordinates - The coordinates.
   * @param {function} onOver - The onOver event.
   */
  constructor(name, type, coordinates, onOver) {
    super(name, type, AREA_RECT, coordinates, onOver)
  }

  /**
   * Draw the area.
   * @param {CanvasRenderingContext2D} ctx - The canvas 2D context.
   */
  draw(ctx) {
    RectArea.draw(ctx, this)
  }

  /**
   * Draw a area.
   * @param {CanvasRenderingContext2D} ctx - The canvas 2D context.
   * @param {RectArea} ctx - The area.
   */
  static draw(ctx, area) {
    ctx.rect(
      area.coordinates.x - Scene.offsetX,
      area.coordinates.y - Scene.offsetY,
      area.coordinates.w,
      area.coordinates.h
    )
  }

  /**
   * Returns if a specified position occurs within the area.
   * @param {Vector} position - The position to seek.
   */
  contains(position) {
    return RectArea.contains(position, this)
  }

  /**
   * Returns if a specified position occurs within a area.
   * @param {Vector} position - The position to seek.
   * @param {RectArea} ctx - The area.
   */
  static contains(position, area) {
    return Rectangle.contains(position, area.coordinates)
  }
}
