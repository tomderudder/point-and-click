'use strict'

/**
 * Manage scenes.
 * @param {Game} game - The game.
 */
class SceneManager {
  /**
   * @constructor
   * @param {Game} game - The game.
   */
  constructor(game) {
    this.game = game
    this.scenes = []
    this.sceneSelected = null

    // DOM
    const btn = document.querySelector('[data-pac-scene-unload]')

    btn.addEventListener('click', () => {
      this.unload()
    })
  }

  /**
   * Add and link a new scene to the game
   * @param {Scene} scene - The scene to add.
   */
  add(scene) {
    scene.game = this.game
    scene.canvas = Game.canvas
    scene.ctx = this.game.ctx
    this.scenes.push(scene)
    this.game.onSceneAdded(scene)
  }

  /**
   * Load the scene in the canvas
   * @param {Scene} scene - The scene to load.
   */
  load(scene) {
    if (scene && scene instanceof Scene) {
      // the scene is valid
      const newSceneSelected = this.scenes[this.scenes.indexOf(scene)]

      if (newSceneSelected) {
        // the scene is fund

        if (this.sceneSelected) {
          // unload the curent scene
          this.sceneSelected.unload()
        }
        // replace the curent scene
        this.sceneSelected = newSceneSelected
        // load the curent scene
        this.sceneSelected.load()
      } else {
        // the scene is not fund
        console.warn('the scene is not fund')
      }
    } else {
      // the scene is not valid
      console.warn('the scene is not valid')
    }
  }

  /**
   * Unload the scene in the canvas
   * @param {Scene} scene - The scene to unload.
   */
  unload() {
    if (this.sceneSelected) {
      this.sceneSelected.unload()
      this.sceneSelected = null
    }
  }

  /**
   * Event handler code then a event is keeping
   * @param {string} name - The name of the event.
   * @param {Event|MouseEvent|KeyboardEvent|DragEvent|WheelEvent} event - The event object.
   */
  on(name, event) {
    this.sceneSelected && this.sceneSelected['on' + name](event)
  }

  /**
   * Synchronizes the state of the audio betwwen the scene and the game
   */
  updateStateAudio() {
    this.sceneSelected && this.sceneSelected.updateStateAudio()
  }
}
