'use strict'

/**
 * Manage events.
 * @param {Game} game - The game.
 */
class EventManager {
  /**
   * @constructor
   * @param {Game} game - The game.
   */
  constructor(game) {
    this.game = game

    Game.canvas.addEventListener('click', this.bind(this.onclick))
    // Game.canvas.addEventListener('dblclick', this.bind(this.ondblclick))
    Game.canvas.addEventListener('mousemove', this.bind(this.onmousemove))
    // Game.canvas.addEventListener('wheel', event => console.log(event));
  }

  /**
   * Bind a externe method to the object
   * @param {function} method - The method to bind.
   */
  bind(method) {
    return event => this[method.name](event)
  }

  /**
   * Event handler code then a click event is keeping
   * @param {MouseEvent} event - The event object.
   */
  onclick(event) {
    this.game.scene.on('click', event)
  }

  /**
   * Event handler code then a double click event is keeping
   * @param {MouseEvent} event - The event object.
   */
  ondblclick(event) {
    this.game.scene.on('dblclick', event)
  }

  /**
   * Event handler code then a mouse down event is keeping
   * @param {Event} event - The event object.
   */
  onmousedown(event) {
    return
  }

  /**
   * Event handler code then a mouse enter event is keeping
   * @param {Event} event - The event object.
   */
  onmouseenter(event) {
    return
  }

  /**
   * Event handler code then a mousse leave event is keeping
   * @param {MouseEvent} event - The event object.
   */
  onmouseleave(event) {
    this.game.scene.on('mousemove', event)
  }

  /**
   * Event handler code then a mousse move event is keeping
   * @param {MouseEvent} event - The event object.
   */
  onmousemove(event) {
    this.game.scene.on('mousemove', event)
  }

  /**
   * Event handler code then a mouse over event is keeping
   * @param {Event} event - The event object.
   */
  onmouseover(event) {
    return
  }

  /**
   * Event handler code then a mouse out event is keeping
   * @param {Event} event - The event object.
   */
  onmouseout(event) {
    return
  }

  /**
   * Event handler code then a mouse up event is keeping
   * @param {Event} event - The event object.
   */
  onmouseup(event) {
    return
  }

  /**
   * Event handler code then a key down event is keeping
   * @param {KeyboardEvent} event - The event object.
   */
  onkeydown(event) {
    return
  }

  /**
   * Event handler code then a key press event is keeping
   * @param {KeyboardEvent} event - The event object.
   */
  onkeypress(event) {
    return
  }

  /**
   * Event handler code then a key up event is keeping
   * @param {KeyboardEvent} event - The event object.
   */
  onkeyup(event) {
    return
  }

  /**
   * Event handler code then a drag start event is keeping
   * @param {DragEvent} event - The event object.
   */
  dragstart(event) {
    return
  }

  /**
   * Event handler code then a drag leave event is keeping
   * @param {DragEvent} event - The event object.
   */
  dragleave(event) {
    return
  }

  /**
   * Event handler code then a drag over event is keeping
   * @param {DragEvent} event - The event object.
   */
  dragover(event) {
    return
  }

  /**
   * Event handler code then a wheel event is keeping
   * @param {WheelEvent} event - The event object.
   */
  wheel(event) {
    return
  }
}
