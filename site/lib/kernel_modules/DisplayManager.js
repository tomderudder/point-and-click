'use strict'

const RESOLUTION_FULLSCREEN = 0
const RESOLUTION_SMALL = 1
const RESOLUTION_MEDIUM = 2
const RESOLUTION_LARGE = 3
const RESOLUTIONS = {
  RESOLUTION_FULLSCREEN: 0,
  RESOLUTION_SMALL: 1,
  RESOLUTION_MEDIUM: 2,
  RESOLUTION_LARGE: 3
}

const SCALE_MODE_RESIZE = 0
const SCALE_MODE_EXPAND = 1
/**
 * Manage display to the application.
 * @param {CanvasRenderingContext2D} ctx - The canvas 2D context.
 */
class DisplayManager {
  /**
   * @constructor
   * @param {CanvasRenderingContext2D} ctx - The canvas 2D context.
   */
  constructor(ctx) {
    // init environement
    Element.prototype.requestFullscreen =
      Element.prototype.requestFullscreen ||
      Element.prototype.webkitRequestFullScreen ||
      Element.prototype.mozRequestFullScreen ||
      Element.prototype.msRequestFullscreen
    document.onfullscreenchange = document.onwebkitfullscreenchange = document.MSFullscreenChange = this.bind(
      this.fullScreenChange
    )
    this.resolutions = [
      null,
      {
        width: 768,
        height: 432,
        scale: 0.6
      },
      {
        width: 1280,
        height: 720,
        scale: 1
      },
      {
        width: 1664,
        height: 936,
        scale: 1.3
      }
    ]
    this.ctx = ctx
    this.ratio = new Vector(1, 1)
    this.scale(RESOLUTION_SMALL)

    // DOM
    const btns = document.querySelectorAll('[data-pac-display]')

    for (let i = 0; i < btns.length; i++) {
      const btn = btns[i]

      btn.addEventListener('click', () => {
        this.scale(RESOLUTIONS[btn.dataset.pacDisplay])
      })
    }
  }

  /**
   * Bind a externe method to the object
   * @param {function} method - The method to bind.
   */
  bind(method) {
    return event => this[method.name](event)
  }

  /**
   * Scale the canvas
   * @param {number} size - The size.
   */
  scale(size) {
    if (size) {
      this.scalMode = SCALE_MODE_RESIZE
      Game.canvas.width = this.resolutions[size].width
      Game.canvas.height = this.resolutions[size].height
      this.ratio = new Vector(this.resolutions[size].scale, this.resolutions[size].scale)
      this.ctx.scale(this.ratio.x, this.ratio.y)
    } else {
      this.scalMode = SCALE_MODE_EXPAND
      this.scaleFullScreen()
      app.scene.sc
    }
    this.setFontsize()
  }

  /**
   * Scale the canvas on fullscreen
   */
  scaleFullScreen() {
    Game.canvas.requestFullscreen()
    Game.canvas.style.height = '100%'
    Game.canvas.width = screen.width
    Game.canvas.height = screen.height
    this.ratio = new Vector(screen.width / 1280, screen.height / 720)
  }

  /**
   * Event handler code then fullscreen change event is keeping
   * @param {Event} event - The event object.
   */
  fullScreenChange(event) {
    if (
      (document.webkitFullscreenElement ||
        document.mozFullScreen ||
        document.msFullscreenElement) === this.canvas
    )
      this.scale(RESOLUTION_MEDIUM)
  }

  /**
   * Initialize the font
   */
  setFontsize() {
    this.ctx.font = '30px Comic Sans MS'
    this.ctx.fillStyle = '#fff'
    this.ctx.textAlign = 'center'
  }
}
